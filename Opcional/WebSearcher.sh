#!/bin/bash

if [ -z "$1" ]
  then
    echo "Provide a string to search for"
    exit 1
fi

curl https://www.marca.com/ > ./marcaContent.txt
lineNumber=$(grep -n "$1" ./marcaContent.txt | head -n 1 | cut -d: -f1)
echo String \""$1"\" found for first time on line $lineNumber